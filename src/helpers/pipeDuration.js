const pipeDuration = (minutes) => {
	let hoursNum = Math.floor(minutes / 60);
	let minutesNum = minutes % 60;

	return (
		(hoursNum <= 9 ? '0' : '') +
		hoursNum +
		':' +
		(minutesNum <= 9 ? '0' : '') +
		minutesNum +
		' hours'
	);
};

export default pipeDuration;
