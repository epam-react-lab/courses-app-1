import { mockedAuthorsList } from '../constants';

const extractAuthorName = (authorId) => {
	const author = mockedAuthorsList.find((author) => authorId === author.id);
	return author ? author.name : '';
};

export default extractAuthorName;
