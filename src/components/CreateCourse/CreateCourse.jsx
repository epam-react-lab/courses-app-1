import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import Textarea from './components/Textarea/Textarea';
import pipeDuration from '../../helpers/pipeDuration';
import dateGeneratop from '../../helpers/dateGeneratop';
import { mockedAuthorsList, mockedCoursesList } from '../../constants';

import './createCourse.css';

const CreateCourse = (props) => {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState(0);
	const [authors, setAuthors] = useState(mockedAuthorsList);
	const [authorName, setAuthorName] = useState('');
	const [courseAuthors, setCourseAuthors] = useState([]);

	const updateTitle = (e) => {
		setTitle(e.target.value);
	};

	const updateDescription = (e) => {
		setDescription(e.target.value);
	};

	const updateDuration = (e) => {
		let value;
		if (typeof Number(e.target.value) === 'number') {
			value = Number(e.target.value);
		}
		if (value <= 0 || value % 1 !== 0) value = 0;

		setDuration(value);
	};

	const updateAuthorName = (e) => {
		setAuthorName(e.target.value);
	};

	const createAuthor = () => {
		if (authorName.length < 2) return;

		const newAuthor = {
			id: uuidv4(),
			name: authorName,
		};

		mockedAuthorsList.push(newAuthor);
		setAuthors([...mockedAuthorsList]);
	};

	const addAuthor = (id) => {
		setCourseAuthors([
			...courseAuthors,
			authors.find((author) => author.id === id),
		]);
	};

	const removeAuthor = (id) => {
		setCourseAuthors([...courseAuthors.filter((author) => author.id !== id)]);
	};

	const isValidCourse = (course) => {
		if (typeof course.id !== 'string') return;
		if (typeof course.title !== 'string' || course.title.trim().length <= 1)
			return;
		if (
			typeof course.description !== 'string' ||
			course.description.trim().length <= 1
		)
			return;
		if (typeof course.duration !== 'number' || course.duration === 0) return;
		if (course.authors.length === 0) return;

		return true;
	};

	const createCourse = () => {
		const course = {
			id: uuidv4(),
			title: title,
			description: description,
			creationDate: dateGeneratop(),
			duration: duration,
			authors: [...courseAuthors.map((author) => author.id)],
		};

		if (!isValidCourse(course)) {
			alert('Please, fill in all fields.');
			return;
		}

		mockedCoursesList.push(course);
		alert('Course created successfully.');
		props.onSuccess();
	};

	return (
		<div className='CreateCourse'>
			<div className='CreateCourse_header'>
				<Input
					labelText='Title'
					placeholderText='Enter title...'
					onChange={updateTitle}
				></Input>
				<Button buttonText='Create course' onClick={createCourse} />
			</div>
			<div className='CreateCourse_header'>
				<Textarea
					labelText='Description'
					placeholderText='Enter description'
					rows={3}
					onChange={updateDescription}
				/>
			</div>
			<div className='CreateCourse_form'>
				<div className='CreateCourse_block'>
					<div className='CreateCourse_block'>
						<h3>Add author</h3>
						<Input
							labelText='Author name'
							placeholderText='Enter author name...'
							onChange={updateAuthorName}
						/>
						<Button buttonText='Create author' onClick={createAuthor} />
					</div>
					<div className='CreateCourse_block'>
						<h3>Duration</h3>
						<Input
							labelText='Duration'
							placeholderText='Enter duration in minutes...'
							onChange={updateDuration}
						/>
						<p>
							Duration: <b>{pipeDuration(duration)}</b>
						</p>
					</div>
				</div>
				<div className='CreateCourse_block'>
					<div className='CreateCourse_block'>
						<h3>Authors</h3>
						{authors
							.filter(
								(author) =>
									courseAuthors
										.map((author) => author.id)
										.indexOf(author.id) === -1
							)
							.map((author) => (
								<div className='CreateCourse_author' key={author.id}>
									<span>{author.name}</span>
									<Button
										buttonText='Add author'
										onClick={() => {
											addAuthor(author.id);
										}}
									/>
								</div>
							))}
					</div>
					<div className='CreateCourse_block'>
						<h3>Course authors</h3>
						{courseAuthors.map((author) => (
							<div className='CreateCourse_author' key={author.id}>
								<span>{author.name}</span>
								<Button
									buttonText='Delete author'
									onClick={() => {
										removeAuthor(author.id);
									}}
								/>
							</div>
						))}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CreateCourse;
