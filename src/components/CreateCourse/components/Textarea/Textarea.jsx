import './textarea.css';

const Textarea = (props) => {
	return (
		<span className='w-100'>
			<label className='Textarea_label' htmlFor={props.id}>
				{props.labelText}
			</label>
			<textarea
				className='Textarea bs-bb'
				id={props.id}
				rows={props.rows}
				placeholder={props.placeholderText}
				onChange={(e) => props.onChange(e)}
			/>
		</span>
	);
};

export default Textarea;
