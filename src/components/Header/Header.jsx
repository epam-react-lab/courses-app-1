import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import './Header.css';

const Header = () => {
	return (
		<div className='Header'>
			<span>
				<Logo height='30px' />
			</span>
			<span className='NavElements'>
				<span>Mykyta</span>
				<Button buttonText='Logout' />
			</span>
		</div>
	);
};

export default Header;
