import { useState } from 'react';

import Courses from '../Courses/Courses';
import CreateCourse from '../CreateCourse/CreateCourse';

const Main = () => {
	const [createMode, setCreateMode] = useState(false);

	const toggleCreateMode = () => {
		setCreateMode(!createMode);
	};

	return createMode ? (
		<CreateCourse onSuccess={toggleCreateMode} />
	) : (
		<Courses addCourseBtnClick={toggleCreateMode} />
	);
};

export default Main;
