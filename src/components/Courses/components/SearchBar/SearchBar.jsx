import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

import './searchBar.css';

const SearchBar = (props) => {
	let searchString = '';

	const updateSearchString = (value) => {
		searchString = value;
	};

	return (
		<div className='SearchBar'>
			<Input
				id='search'
				placeholderText='Enter course name...'
				onChange={(e) => updateSearchString(e.target.value)}
			/>
			<Button
				buttonText='Search'
				onClick={() => props.onSearch(searchString)}
			/>
		</div>
	);
};

export default SearchBar;
