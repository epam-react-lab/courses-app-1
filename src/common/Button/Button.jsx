import './Button.css';

const Button = (props) => {
	return (
		<button className='Button' onClick={props.onClick}>
			{props.buttonText}
		</button>
	);
};

export default Button;
