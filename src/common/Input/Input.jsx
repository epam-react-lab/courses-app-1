import './input.css';

const Input = (props) => {
	return (
		<span className='bs-bb w-100'>
			<label className='Input_label' htmlFor={props.id}>
				{props.labelText}
			</label>
			<input
				className='Input'
				id={props.id}
				type='text'
				placeholder={props.placeholderText}
				onChange={(e) => props.onChange(e)}
			/>
		</span>
	);
};

export default Input;
